//<debug>
Ext.Loader.setPath({
    'Ext': 'touch/src',
    'bShift': 'app',
 
        'Ext.ux': 'touch/src/ux'
   
});
//</debug>

Ext.application({
    models: ["Referral"],
    userData: null,
    controllers: ["Filter","MapPanel","Dashboard","Login","Referral"],
    stores:["Referrals"],

    name: 'bShift',

    requires: [
        'Ext.MessageBox',
        'Ext.data.proxy.Rest',
        'Ext.Img',
        'Ext.Map',
        'Ext.SegmentedButton',
        'Ext.field.Select',
        'Ext.Label',
        'Ext.ux.panel.PDF'
        
    ],

    views: ['Main','Login','Referral','Dashboard','Map','CustomerList','MapPanel','Results','MapPanelCards','Help','About','Filter','Pdf',
    'Ext.ux.panel.PDF'],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function() {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();
 Ext.define('App.override.event.recognizer.Tap', {
    override: 'Ext.event.recognizer.Tap',

    handledEvents: ['tap', 'tapcancel'],

    onTouchStart: function(e) {
        if (this.callSuper(arguments) === false) {
            return false;
        }

        this.startPoint = e.changedTouches[0].point;
    },

    onTouchMove: function(e) {
        var touch = e.changedTouches[0],
            point = touch.point,
            moveDistance = 40;

        if (Math.abs(point.getDistanceTo(this.startPoint)) >= moveDistance) {
            this.fire('tapcancel', e, [touch], {
                touch: touch
            });
            return this.fail(this.self.TOUCH_MOVED);
        }
    },

    onTouchEnd: function(e) {
        var touch = e.changedTouches[0];

        this.fire('tap', e, [touch], {
            touch: touch
        });
    }
});
        // Initialize the main view
        Ext.Viewport.add(Ext.create('bShift.view.Login'));
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
