Ext.define('bShift.view.Pdf',{  
//  extend:'Ext.ux.panel.PDF',
  extend:'Ext.Panel',
  xtype:'pdfview',
  
  config:{ layout    : 'fit',
    items:[
    
     {
                xtype : 'titlebar',
                docked : 'top',
             title : 'PDF',
            height:'3.3em',
                items : [{
                    xtype : 'button',
                    text : 'Back',
                    id:'backToHelpButton',
                    ui:'back',
                    align : 'left'
                }]
          },
    {   xtype:'pdfpanel',
        fullscreen: true,
           
         //  src       : 'assets/test.pdf', // URL to the PDF - Same Domain or Server with CORS Support
           src:  'http://cdn.mozilla.net/pdfjs/tracemonkey.pdf',
            style     : {
                backgroundColor: '#333'
          },
          width:'100%',
          height:'100%'
          }
 
 ]
          
    }
    
    });
