Ext.define('bShift.view.Filter',{
    extend:'Ext.Panel',
    xtype:'filter',
    
     config:{
        fullscreen:true,
      
        items:[   
         {
                xtype : 'titlebar',
                docked : 'top',
                 title : '<img src="assets/imgs/toppanel/filter.png"/>',
                height:'3.3em',
                items : [{
                    xtype : 'button',
                    text : 'Back',
                    id:'backToMapButton',
                    ui:'back',
                    align : 'left'
                }]
          },
          {
              xtype:'panel',
                margin:'10%',
              layout:{
                  type:'vbox'
              },
              items:[
              
                  {
                      xtype:'label',
                      html:'Team Name',
                      style:{'margin-bottom':'5px'}
                  },
                  {
                    xtype: 'selectfield',
                    style:{'margin-bottom':'20px'},
                    
                    inputCls:'whiteLabel',
                 
                    options: [
                        {text: 'All Teams',  value: 'first'},
                        {text: 'My Team', value: 'second'},
                        {text: 'Another Team',  value: 'third'}
                    ]
                },
                 
                  {
                      xtype:'label',
                      html:'Organization Name',
                      style:{'margin-bottom':'5px'}
                  },
                  {
                    xtype: 'selectfield',
                    style:{'margin-bottom':'20px'},
                      inputCls:'whiteLabel',
                    options: [
                        {text: 'All organization',  value: 'first'},
                        {text: 'My organization', value: 'second'},
                        {text: 'Another organization',  value: 'third'}
                    ]
                },
                
                  {
                      xtype:'label',
                      html:'Interest',
                      style:{'margin-bottom':'5px'}
                  },
                  {
                    xtype: 'selectfield',
                    style:{'margin-bottom':'20px'},
                      inputCls:'whiteLabel',
                    options: [
                        {text: 'All',  value: 'first'},
                        {text: 'Another', value: 'second'},
                        {text: 'Yet Another',  value: 'third'}
                    ]
                }
                
                
                
                
                ]
          },
                {    
                    xtype:'button',
                    text:'Filter',
                    bottom:'0',
                    width:'80%',
                      margin:'10%'
                }
          ]
         }
});
