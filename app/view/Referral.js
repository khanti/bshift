Ext.define('bShift.view.Referral',{
    extend:'Ext.form.Panel',
    xtype:'referralpanel',
    
    requires:[
        'Ext.form.FieldSet',
    ],
    
    config:{
        title:'Referral',
        
        items:[
        {
            xtype : 'titlebar',
            docked : 'top',
              title : '<img src="assets/imgs/toppanel/referral.png"/>',
            height:'3.3em',
            items : [{
                xtype : 'button',
                text : 'Back',
                id:'backButton',
                ui:'back',
                align : 'left'
            }]
        }, 
          
   
             {
            xtype: 'label',
            html: '<span style="color:white; margin:10px;">Please insert your data here.</span><span style="color:red; font-size:0.9em;">*required</span><hr style="border: 2px solid #1b1b1d; border-bottom: 1px solid #333439; margin:10px;"/>'
         },
            {
                xtype:'textfield',
                name:'firstname',
                placeHolder:'First Name*', 
                required:true,
                cls:'referraInput'
            },
            {
                xtype:'textfield',
                name:'lastname',
                 placeHolder:'Last Name*',
                required:true,
                cls:'referraInput'
            },
            {
                xtype:'textfield',
                name:'phonenumber',
              placeHolder:'Phone number*'  ,
                required:true  ,
                cls:'referraInput'
            },
            {
                xtype:'textfield',
                name:'email',
                cls:'referraInput',
              placeHolder:'Email'
            },
             {
                xtype:'textfield',
                name:'address1',
                cls:'referraInput',
                placeHolder:'Address 1'
            },
             {
                xtype:'textfield',
                name:'address2',
                cls:'referraInput',
                placeHolder:'Address 2'
            },
             {
                xtype:'textfield',
                name:'city',
                cls:'referraInput',
                placeHolder:'City'
            },
             {
                xtype:'textfield',
                name:'state',
                cls:'referraInput',
                placeHolder:'State'
            },
             {
                xtype:'textfield',
                name:'zipcode',
                cls:'referraInput',
                placeHolder:'Zip code'
            },
            {
                xtype:'panel',
                layout:{
                    type:'hbox',
                    pack:'center',
                    align:'center'
                },
                items:[
                     {
                        xtype: 'checkboxfield',
                        name : 'HVAC',
                        value: 'HVAC',
                        label: 'HVAC',
                        labelWidth: '70%',
                        labelAlign: 'right',
                        checked:false
                     },
                     {
                        xtype: 'checkboxfield',
                        name : 'Windows',
                        value: 'Windows',
                        label: 'Windows',
                        labelWidth: '70%',
                        labelAlign: 'right',
                        checked:false
                     }, 
                    {
                        xtype: 'checkboxfield',
                        name : 'HEA',
                        value: 'HEA',
                        label: 'HEA',
                        labelWidth: '70%',
                        labelAlign: 'right',
                        checked:false
                     }
                ]
            },{
                xtype:'panel',
                layout:{
                    type:'hbox',
                    pack:'center',
                    align:'center'
                },
                items:[
                     {
                        xtype: 'checkboxfield',
                        name : 'Solar',
                        value: 'Solar',
                        label: 'Solar',
                        labelWidth: '70%',
                        labelAlign: 'right',
                        checked:false
                     },
                     {
                        xtype: 'checkboxfield',
                        name : 'Weather',
                        value: 'Weather',
                        label: 'Weather',
                        labelWidth: '70%',
                        labelAlign: 'right',
                        checked:false
                     }, 
                    {
                        xtype: 'checkboxfield',
                        name : 'Other',
                        value: 'Other',
                        label: 'Other',
                        labelWidth: '70%',
                        labelAlign: 'right',
                        checked:false
                     }
                ]
            },
            
             {
                xtype:'button',
                id:'photoButton',
                text:'Take Photo',
                style:{'margin-bottom':'10px'}
            },
            
            {
                xtype:'button',
                id:'submitButton',
                text:'Submit'
            }
        ]
    }
    
});
