Ext.define('bShift.view.Main', {
    extend: 'Ext.Panel',
    xtype: 'main',
 
    
    requires: [
      
    ],
    config: {   
       layout:'card',
        animation: {
            type: 'slide',
            direction: 'left'
        },
       fullscreen:true,
       items:[
        {
           xtype:'loginpanel'
        },
        {
           xtype: 'referralpanel'
        }
       ]
    }
});
