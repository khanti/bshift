Ext.define('bShift.view.Results',{
    extend:'Ext.Panel',
    xtype:'results',
    requires:['Ext.TitleBar'],
    config:{ 
        scrollable:true,
              html:

'<div id="resultsWrapper"><div id="resultsUserDetails"><img id="userPhoto"/><div id="userData"><p id="userName">Jane Doe</p><p class="userDetailsP">Address</p></div></div><div class="collapseDiv" ><div class="collapseHeader" id="chReferrals"><img src="assets/imgs/map/icon1.png" class="collapseIcon"/>Referrals <img class="iconArrowLeft" src="assets/imgs/map/arrow_down.png"/></div><hr/><div class="collapseContentDiv"><ul><li>One</li><li>Two</li></ul></div></div><div class="collapseDiv" ><div class="collapseHeader" id="chCustomers"><img src="assets/imgs/map/icon2.png" class="collapseIcon"/>Customers <img class="iconArrowLeft" src="assets/imgs/map/arrow_down.png"/></div><hr/><div class="collapseContentDiv"><ul><li>One me</li><li>Two me</li></ul></div></div><div class="collapseDiv" ><div class="collapseHeader" id="chConversion"><img src="assets/imgs/map/icon3.png" class="collapseIcon"/>Conversion <img class="iconArrowLeft" src="assets/imgs/map/arrow_down.png"/></div><hr/><div class="collapseContentDiv"><ul><li>One me</li><li>Two me</li></ul></div></div><div class="collapseDiv" ><div class="collapseHeader" id="chRewards"><img src="assets/imgs/map/icon4.png" class="collapseIcon"/>Rewards <img class="iconArrowLeft"src="assets/imgs/map/arrow_down.png"/></div><hr/><div class="collapseContentDiv"><ul><li>One me</li><li>Two me</li></ul></div></div><div class="collapseDiv" ><div class="collapseHeader" id="chPoints"><img src="assets/imgs/map/icon5.png" class="collapseIcon"/>Points <img class="iconArrowLeft"src="assets/imgs/map/arrow_down.png"/></div><hr/><div class="collapseContentDiv"><ul><li>One me</li><li>Two me</li></ul></div></div><div class="collapseDiv" ><div class="collapseHeader" id="chEnergy"><img src="assets/imgs/map/icon6.png" class="collapseIcon"/>Energy <img class="iconArrowLeft" src="assets/imgs/map/arrow_down.png"/></div><hr/><div class="collapseContentDiv"><ul><li>One me</li><li>Two me</li></ul></div></div></div>'


   , listeners: {
      
     
                activate: function() {
                    
                    
   
             
       
             Ext.select('#userPhoto').elements[0].src = bShift.app.userData.photo  ;
              Ext.select('#userName').elements[0].innerHTML = bShift.app.userData.name  ;
     
           var divs =     Ext.select('.collapseContentDiv');
           
             
             divs.each( function (item) {
              console.log(  item.dom.parentElement.getElementsByClassName('iconArrowLeft'));
                if(item.dom.parentElement.firstChild.id == "chReferrals" ){
                    item.dom.parentElement.getElementsByClassName('iconArrowLeft')[0].hidden = false;
                    
                  item.setVisible(true);
                  }
                else{
                        item.dom.parentElement.getElementsByClassName('iconArrowLeft')[0].hidden = true;
                        item.setVisible(false);
                }
                 
                });

            this.element.on({
               tap: function(e){
              //    console.log( e.target.id);
             var divs =     Ext.select('.collapseContentDiv');
           //  console.log(divs);
             
             divs.each( function (item) {
                console.log(item.dom.parentElement.firstChild.id + " " + e.target.id );
                if(item.dom.parentElement.firstChild.id ==  e.target.id ){
                           item.dom.parentElement.getElementsByClassName('iconArrowLeft')[0].hidden = false;
                  item.setVisible(true);
                  }
                else{
                     item.setVisible(false);
                            item.dom.parentElement.getElementsByClassName('iconArrowLeft')[0].hidden = true;
                }
                
                });
        
             ///.setVisible(false);
               //    Ext.select('.collapseContentDiv').setVisible(true);
                 // Ext.select('#'+e.target.id +' .collapseContentDiv').setVisible(true);
               },
               delegate: '.collapseHeader'
            });
             
         }
               
           
        
    }
    
   
     }
});
