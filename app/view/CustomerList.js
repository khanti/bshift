Ext.define('bShift.view.CustomerList',{
    extend:'Ext.dataview.List',
    xtype:'customerlist',
  
    config:{
   
        store:'Referrals',
        loadingText:'Loading...',
        
        emptyText:'No referrals found.',
        
        itemTpl:'<div class="userListItem">  <div class="userPhoto"></div>    <div class="userDetailsDiv"><p>{firstname} {lastname}</p> <img src="assets/imgs/heart.png" class="smallImg"/> <span>Windows</span><img src="assets/imgs/phone.png" class="smallImg"/> <span>{phonenumber}</span><img src="assets/imgs/date.png" class="smallImg"/> <span>08:00AM</span></div></div><div class="clear:both;"><img class="smallImg" src="assets/imgs/pin.png" /><span class="userAdressDiv">{address1} {address2} {city}{zipcode}</span></div> '   
}
});
