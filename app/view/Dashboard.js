Ext.define('bShift.view.Dashboard', {
    extend : 'Ext.Panel',
    xtype : 'dashboard',

    config : {
        fullscreen:true,
       layout : {
                type : 'vbox',
                align : 'stretch',
                pack : 'center'
            },
        items : [
        {
            xtype : 'titlebar',
            docked : 'top',
            title : '<img src="assets/imgs/toppanel/bshift.png"/>',
            height:'3.3em',
           items : [
            
            {
                  xtype : 'button',  text : 'Logout', align : 'right'
               
            }
            ]
        }, 
        {
            xtype:'panel',
            cls:'gridBG',
            items:[
                    {
                    xtype:'container',
                
                        layout : {
                        type : 'hbox',
                        align : 'stretch',
                        pack : 'center'
                     },
                         items: [{ xtype : 'image',   src: 'assets/imgs/dashboard/referral.png',  height: 100, width: 100 , id:'showReferralViewButton',margin:25},
                                 { xtype : 'image',  src: 'assets/imgs/dashboard/map.png',   height: 100,width: 100, id:'showMapViewButton',margin:25 }
                            ]
                      },
                      {
                        xtype:'container',
                        layout : {
                        type : 'hbox',
                        align : 'stretch',
                        pack : 'center'
                     },
                         items: [{ xtype : 'image',   src: 'assets/imgs/dashboard/results.png',  height: 100, width: 100 , id:'showResultslViewButton',margin:25},
                                 { xtype : 'image',  src: 'assets/imgs/dashboard/facebook.png',   height: 100,width: 100, id:'showFacebookViewButton',margin:25 }
                            ]
                      },
                    {
                         xtype:'container',
                        layout : {
                        type : 'hbox',
                        align : 'stretch',
                        pack : 'center'
                     },
                         items: [{ xtype : 'image',   src: 'assets/imgs/dashboard/twitter.png',  height: 100, width: 100 , id:'showTwitterViewButton',margin:25},
                                 { xtype : 'image',  src: 'assets/imgs/dashboard/help.png',   height: 100,width: 100, id:'showHelpViewButton',margin:25 }
                            ]
                  }
            ]    
        }

            ]

    }

});
