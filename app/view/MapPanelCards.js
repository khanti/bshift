Ext.define('bShift.view.MapPanelCards',{
    extend:'Ext.Panel',
    xtype:'mappanelcards',
    
    config:{
            layout: {type:'card'},
              style:'width:100%; height:100%;',
                items: [
                 {
                     xtype:'panel',
                     
                     items:[
                    {
                      xtype: 'selectfield',
                         style:{'margin':'20px'},
                        inputCls:'whiteLabel',
                        options: [
                        {text: 'Show All',  value: 'first'},
                        {text: 'Show Others', value: 'second'},
                        {text: 'Show More',  value: 'third'}
                          ]
                     },
                     {  
                         xtype:'map',
                         width:'100%',
                         height:'100%'
                        // ,             style:{'padding-left':'10px','padding-right':'10px'}
                         
                         }
                     ]
                     
                       
                 },
                    {
                        xtype:'customerlist'
                    },
                    {
                        xtype: 'results'
                    }
                ]
    },
    
});
