Ext.define('bShift.view.Map',{
    extend:'Ext.Map',
    xtype:'map',
    id:'map',
    config:{ 
        
                
                   
        
            useCurrentLocation:true,
             mapOptions: {
            zoom: 13 
           }         ,
     
              listeners: {
            delay: 500,
            maprender: function( me, gmap, eOpts){
                
             me = this;
                var lat, lng;
                var geoLocationOptions = { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true };
                navigator.geolocation.getCurrentPosition(geoLocationSuccess,geoLocationError,geoLocationOptions);

                function geoLocationSuccess(position) {
                    lat = position.coords.latitude;
                    lng = position.coords.longitude;
                  // Ext.Msg.alert("Geolocation","Latitude:"+ lat +", Longitude:"+ lng); 
                 
                  var referralsStore = Ext.getStore('Referrals');
                  
                  referralsStore.each(function (item, index, length) {
                      console.log(item.get('latitude') + " " + item.get('longitude'));
                      
                    var marker = new google.maps.Marker({
                          position: new google.maps.LatLng(item.get('latitude'), item.get('longitude')),
                         map: gmap,
                          animation: google.maps.Animation.DROP,
                          storeItem:item
                         });
                             var contentString = 'hej';
               
               

 
        var myOptions = {
             content:""
            ,disableAutoPan: false
            ,maxWidth: 0
            ,pixelOffset: new google.maps.Size(-84, -260)
            ,zIndex: null
            ,boxStyle: { 
                  background: "url('assets/imgs/map_tip.png') no-repeat"
                 // ,opacity: 0.75
                  ,width: "293px"
                  ,height:"247px"
                 }
            ,closeBoxMargin: "10px 2px 2px 2px"
         ,closeBoxURL: ""
            ,infoBoxClearance: new google.maps.Size(1, 1)
            ,isHidden: false
            ,pane: "floatPane"
            ,enableEventPropagation: false
        };
   
        google.maps.event.addListener(marker, "click", function (e) {
          //   console.log(btoa(marker.storeItem.get("photo")));return;
         //   console.log(marker.storeItem.get("photo"));
            var boxText = document.createElement("div");
        //    boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background: yellow; padding: 5px;";
            boxText.innerHTML =
             '<img class="ibPhoto" src="' +marker.storeItem.get("photo64")+ '" />'
           +
              '<div class="ibUser"><p class="ibUserName">'+marker.storeItem.get("firstname") + ' '+ marker.storeItem.get("lastname") 
            + '<p class="ibSmallText">Windows</p><p class="ibSmallText">'+ marker.storeItem.get("phonenumber") +'</p>'
            + '</p></div>'
            +'<div class ="ibAddress">' + marker.storeItem.get("address1") + ' ' +  marker.storeItem.get("address2")  + ' '
            +  marker.storeItem.get("city")  + ' ' +  marker.storeItem.get("state") + ' ' +  marker.storeItem.get("zipcode") 
            + '</div>';
            
            console.log( boxText.outerHTML);
            
            me.ib.setContent( boxText.outerHTML),
            me.ib.open(gmap, marker);
        });
            google.maps.event.addListener(marker, 'mouseout', function() {
                  //   me.ib.close();
                });
        me.ib = new InfoBox(myOptions);
       
               
               /*                 
                var infoWindow = new google.maps.InfoWindow({
                  content: contentString
                });
            
                google.maps.event.addListener(marker, 'click', function() {
                         infoWindow.content = marker.storeItem.get('firstname') + " " + marker.storeItem.get('lastname') ;
                       infoWindow.open(map,marker);
                });  
                
                
                  google.maps.event.addListener(marker, 'mouseout', function() {
                      infoWindow.close();
                });  
                
                */
                
                    });
                 
                
                
              
/*             

*/
    
                
               
                }

               function geoLocationError() {
                   Ext.Msg.alert('Error','Error while getting geolocation');
                }
                
                   
              //   console.log(getComponent('#map'));
              //  console.log(this.getComponent('#map'));
                // console.log(this.getComponent('map'));
             //   console.log(this.map);
            }
        }
                        
                    
    }
});
