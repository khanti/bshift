Ext.define('bShift.view.Help',{
    extend:'Ext.Panel',
    xtype:'helpitem',
    
    config:{
        items:[   
         {
                xtype : 'titlebar',
                docked : 'top',
                  title : '<img src="assets/imgs/toppanel/help.png"/>',
            height:'3.3em',
                items : [{
                    xtype : 'button',
                    text : 'Back',
                    id:'backButton',
                    ui:'back',
                    align : 'left'
                }]
          },
          {
              xtype:'button',
              text:'Help 1',
              id:'showHelp1',
              margin:'15 20 15 20'
          },
          {
              xtype:'button',
              text:'Help 2',
              id:'showHelp2',
              margin:'15 20 15 20'
          },
          {
              xtype:'button',
              text:'About',
              id:'showAboutViewButton',
              margin:'15 20 15 20'
          }
          ]
    }
});
