Ext.define('bShift.view.MapPanel',{
    extend:'Ext.Panel',
    xtype:'mappanel',
    requires:['Ext.TitleBar'],
    
    config:{
            fullscreen:true,
        items:[   
         {
                xtype : 'titlebar',
                docked : 'top',
               title : '<img src="assets/imgs/toppanel/map.png"/>',
            height:'3.3em',
                items : [{
                    xtype : 'button',
                    text : 'Back',
                    id:'backButton',
                    ui:'back',
                    align : 'left'
                },
                {
                    xtype:'button',
                    text:'Filter',
                    id:'filterButton',
                    align:'right'
                }]
          },
          {
              xtype:'segmentedbutton',
              id:'optionButtons',
              allowDepress:false,
              layout:{pack:'center'},
              padding:10,
         
              items:[
                {text:"Map", id:"mapOption", pressed:true , width:'30%'},
                {text:"Customers", id:"customersOption", width:'30%'},
                {text:"Results", id:"resultsOption", width:'30%'}
              ]
          },
          
          {
            xtype:'mappanelcards' ,
            id:'cardWrapper'  
          }
          
             
                    
            ]
    }
});
