 
Ext.define('bShift.view.Login',{
    extend:'Ext.form.Panel',
    xtype:'loginpanel',
  
    me:this,
    requires:[
        'Ext.form.FieldSet',
        'Ext.form.Email',
        'Ext.form.Password'
    ],
    
    config:{
         layout: {
        type: 'vbox',
        align: 'stretch',
        pack: 'center'
    },
    fullscreen:true,
    scrollable:false,
        items:[
       /* {
           
              xtype:'textfield',
              name:'phonenumber',
              label:'Phone Number',
              margin:10
        },
        {
            xtype:'passwordfield',
            name:'password',
            label:'Password'
        },*/
        {
            xtype:'image',
            src: 'assets/imgs/login/large_logo.png',
            height:50,
            style:{'margin-bottom':'50px', 'max-width': '100%' }
         
            
        },
        {
            xtype:'button',
            text:'Login with Facebook',
            id:'loginButton',
      
            handler:function(){
              //  this.up('main').setActiveItem(2);
              
          //      this.bShift.app.fireEvent("logMeIn",this);
            },
            scope:this
        }
        ]
    }
    
    
    
});
