Ext.define('bShift.controller.MapPanel', {
    extend: 'Ext.app.Controller',
    
    config: {
        refs: {
            mapPanel:'mappanel'
        },
        control: {
            '#mapOption':{
                tap : 'showMap'
            },
            '#customersOption':{
                tap : 'showCustomers'
            },
            '#resultsOption':{
                tap : 'showResults'
            },
            '#filterButton':{
                tap: 'showFilter'
            }
        }
    },
    
    //called when the Application is launched, remove if not needed
    launch: function(app) {
        
    },
    showMap:function(){
        this.getMapPanel().getComponent('cardWrapper').setActiveItem(0);
       
    },
    showCustomers:function(){
         this.getMapPanel().getComponent('cardWrapper').setActiveItem(1);
    },
    showResults:function(){
       this.getMapPanel().getComponent('cardWrapper').setActiveItem(2);
    },
    showFilter:function(){
       var me = this;
        
        if(!me.filterView){
            me.filterView = Ext.create('bShift.view.Filter',{ id:'filterView'});
        }
    
      
        Ext.Viewport.animateActiveItem(me.filterView, this.slideLeftTransition);
    },
       slideLeftTransition : {
        type : 'slide',
        direction : 'left'
    }
});