Ext.define('bShift.controller.Dashboard', {
    extend: 'Ext.app.Controller',
    
    config: {
        refs: {
                   referralpanel:'referralpanel'
        },
        control: {
             '#showReferralViewButton' : {
                tap : 'showReferralView'
            },
            '#showMapViewButton':{
                tap:'onShowMapView'
            },
            
            '#showResultslViewButton':{
                   tap:'onShowResultsView' 
            },
            '#showHelpViewButton':{
              tap:'onShowHelpView'  
            },
            '#backToHelpButton':{
              tap:'onShowHelpViewBack'  
            },
            '#showAboutViewButton':{
              tap:'onShowAboutView'  
            },
            '#backToMapButton':{
                tap:'onBackToMapPane'
            },
            '#showHelp1':{
              tap:'showPDF'  
            },
            '#showHelp2':{
              tap:'showPDF'  
            },
             referralpanel:{
             'showMapView':'onShowMapView'
           
             }
           
        },listeners : {
           
        }
    },
    
    //called when the Application is launched, remove if not needed
    launch: function(app) {

    },
    showReferralView:function(){
        var me = this;
        
        if(!me.referralView){
            me.referralView = Ext.create('bShift.view.Referral',{ id:'referralView'});
        }
    
        me.referralView.reset();
        Ext.Viewport.animateActiveItem(me.referralView, this.slideLeftTransition);
    },
    onShowMapView:function(){
        var me = this;
        
        if(!me.mapView){
            me.mapView = Ext.create('bShift.view.MapPanel',{ id:'mapPanelView'});
        }
    
      
        Ext.Viewport.animateActiveItem(me.mapView, this.slideLeftTransition); 
        me.mapView.getComponent('optionButtons').setPressedButtons(0);
        me.mapView.getComponent('cardWrapper').setActiveItem(0);
    },
    onBackToMapPane:function(){
        var me = this;
        
        if(!me.mapView){
            me.mapView = Ext.create('bShift.view.MapPanel',{ id:'mapPanelView'});
        }
         
        Ext.Viewport.animateActiveItem(me.mapView, this.slideRightTransition); 
         
    },
    onShowResultsView:function(){
            var me = this;
        
        if(!me.mapView){
            me.mapView = Ext.create('bShift.view.MapPanel',{ id:'mapPanelView'});
        }
    
      
        Ext.Viewport.animateActiveItem(me.mapView, this.slideLeftTransition); 
        me.mapView.getComponent('optionButtons').setPressedButtons(2);
        me.mapView.getComponent('cardWrapper').setActiveItem(2);
    },
    onShowHelpView:function(){
        
       // navigator.notification.alert('Y!');
        var me = this;
        
        if(!me.helpView){
            me.helpView = Ext.create('bShift.view.Help',{ id:'helpView'});
        }
    
      
        Ext.Viewport.animateActiveItem(me.helpView, this.slideLeftTransition); 
    },
    showPDF:function(){
        
        // window.open('http://cdn.mozilla.net/pdfjs/tracemonkey.pdf', '_blank', 'location=yes');
         
      //   return;
          var me = this;
        
        if(!me.helpPdf){
            me.helpPdf = Ext.create('bShift.view.Pdf',{ id:'helpPdf'});
        }
    
      
        Ext.Viewport.animateActiveItem(me.helpPdf, this.slideLeftTransition); 
    },
    onShowAboutView:function(){
             var me = this;
        
        if(!me.aboutView){
            me.aboutView = Ext.create('bShift.view.About',{ id:'aboutView'});
        }
    
      
        Ext.Viewport.animateActiveItem(me.aboutView, this.slideLeftTransition);    
    },
   
    onShowHelpViewBack:function(){
        var me = this;
        
        if(!me.helpView){
            me.helpView = Ext.create('bShift.view.Help',{ id:'helpView'});
        }
    
      
        Ext.Viewport.animateActiveItem(me.helpView, this.slideRightTransition); 
    },
    slideLeftTransition : {
        type : 'slide',
        direction : 'left'
    }
    ,
    slideRightTransition : {
        type : 'slide',
        direction : 'right'
    }
});