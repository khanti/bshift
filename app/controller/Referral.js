Ext.define('bShift.controller.Referral', {
    extend: 'Ext.app.Controller',
    id:'referralController',
    
    config: {
        refs: {
            dashboard:'dashboard',
            referralpanel:'referralpanel'
        },
        control: {
            'referralpanel #submitButton':{
                tap:"submitReferral"
            } ,
            '#photoButton':{
              tap:'takePhoto'  
            }
        }
    },
    
    //called when the Application is launched, remove if not needed
    launch: function(app) {
     this.fireEvent('showMapView',this);
    },
    init:function(app){
       
    },
    submitReferral:function(){
     //   bShift.app.getController('Dashboard').onShowMapView() 
   var me = this;
          var lat, lng;
          var geoLocationOptions = { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true };
           navigator.geolocation.getCurrentPosition(geoLocationSuccess,geoLocationError,geoLocationOptions);

                function geoLocationSuccess(position) {
                    lat = position.coords.latitude;
                    lng = position.coords.longitude;
                    
                        var values = me.getReferralpanel().getValues();
                        
                        values.referringdate = new Date();
                       values.latitude = lat;
                       values.longitude = lng;
                       
                       if(values.HEA == null) values.HEA = true;
                       else                     values.HEA = false;
                       
                           if(values.HVAC == null) values.HVAC = true;
                       else                     values.HVAC = false;
                       
                           if(values.Other == null) values.Other = true;
                       else                     values.Other = false;
                       
                           if(values.Solar == null) values.Solar = true;
                       else                     values.Solar = false;
                      
                               if(values.Weather == null) values.Weather = true;
                       else                     values.Weather = false; 
                   
                        if(values.Windows == null) values.Windows = true;
                       else                     values.Windows = false;     
                       
                       values.photo = this.tempPhoto ;
                     console.log(values);
                     

                     
                        var referral = Ext.create('bShift.model.Referral',values);
                       
                        var errors = referral.validate();
                       // alert(errors.isValid());
                        console.log(errors);
                        
                        if(!errors.isValid()){
                            Ext.Msg.alert('Wait!', errors.getAt(0).getMessage(), Ext.emptyFn);
                            referral.reject();
                            return;
                       }
                       
                        var referralsStore = Ext.getStore('Referrals');
                        
                        referralsStore.add(referral);
                        
                              
                        me.getReferralpanel().fireEvent('backToDashboard',me);
                        referralsStore.sync();
        }
        
        function geoLocationError(){
            alert('sorry there is an error!');
        }
        // referral.save();
    },
             takePhoto:function(){
                 navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
            destinationType: Camera.DestinationType.DATA_URL
         }); 
        
        function onSuccess(imageData) {
            this.tempPhoto =  imageData;
           // var image = document.getElementById('myImage');
           // image.src = "data:image/jpeg;base64," + imageData;
        }
        
        function onFail(message) {
            alert('Failed because: ' + message);
        }


     }  
});