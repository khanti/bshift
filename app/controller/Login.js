Ext.define('bShift.controller.Login', {
    extend : 'Ext.app.Controller',

    config : {
        refs : {
            loginpanel : 'loginpanel',
            referralpanel : 'referralpanel'
        },
        control : {
            '#loginButton' : {
                tap : 'logMeIn'
            },
            '#backButton':{
                tap:'onBackToDashboard'
            },
            referralpanel:{
                  'backToDashboard':'onBackToDashboard'
            }
        }
    },

    slideLeftTransition : {
        type : 'slide',
        direction : 'left'
    },
       slideRightTransition : {
        type : 'slide',
        direction : 'right'
    },
    
    onBackToDashboard:function(){
        var me = this;
        
        if (!me.dashboard) {
         me.dashboard = Ext.create('bShift.view.Dashboard', {
         id: 'dashboard'
         });
         }
          Ext.Viewport.animateActiveItem(me.dashboard, this.slideRightTransition);
       
    },
    logMeIn : function() {
        var me = this;
/*
   if (!me.dashboard) {
         me.dashboard = Ext.create('bShift.view.Dashboard', {
         id: 'dashboard'
         });
         }
                Ext.Viewport.setActiveItem(me.dashboard);
                var referralsStore = Ext.getStore('Referrals');
                Ext.getStore('Referrals').getProxy().setExtraParam('user_id',"100000595045346");    
                Ext.getStore('Referrals').getProxy().setExtraParam('access_token',"AAACG4AQLn9sBAAhSGFO8Nvv0xvWpWjCcSLlzGCU6WF7kTyHMu2Bz4j2GfvKLJr7YxRZBqX0dCf64WXEpZAZARYUDoeW7ZBDdIse3xiI43Ib0Q5CMT24t");

                
                referralsStore.load({
                  callback: function(records, operation, success) {
                        console.log(records);
                        
                        
                        
                        }, scope: this} ); 
                        
          bShift.app.userData = new Object();
           bShift.app.userData.photo = 'https://fbcdn-profile-a.akamaihd.net/static-ak/rsrc.php/v2/yo/r/UlIqmHJn-SK.gif'    ; 
           bShift.app.userData.name = "Piotr Ostiak"; 
          //      Ext.getStore('Runs').load();
          return;
   */
  console.log('Welcome! wation.... ');
        if ((typeof cordova == 'undefined') && (typeof Cordova == 'undefined')) alert('Cordova variable does not exist. Check that you have included cordova.js correctly');
            if (typeof CDV == 'undefined') alert('CDV variable does not exist. Check that you have included cdv-plugin-fb-connect.js correctly');
            if (typeof FB == 'undefined') alert('FB variable does not exist. Check that you have included the Facebook JS SDK file.');
     /*
           FB.login(
                         function(response) {
                         if (response.session) {
                         alert('logged in');
                         } else {
                         alert('not logged in');
                         }
                         },
                         { scope: "email" }
                         );
       
*/  
        FB.login(function(response_log) {
          
            if (response_log.authResponse) {
                console.log('Welcome!  Fetching your information.... ');
                FB.api('/me', { fields: 'id, name, picture' }, function(response_me) {
                        FB.getLoginStatus(function(response) {
                          //  console.log(FB.getAccessToken());
                          //  console.log("id: " + response_me.id)
                             // alert(response_me.id);
                        if (response.status == 'connected') {
                            //alert(response.authResponse.userID);
                            me.onLogin(response_me.id, FB.getAccessToken());
                                bShift.app.userData = new Object();
                        //   alert( response_me.picture.data.url);
                             bShift.app.userData.photo = response_me.picture.data.url;
                              bShift.app.userData.name = response_me.name; 
                        } else {
                          alert("Can't connet with FB.")
                           
                        }
                    });
                });
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }

        }  ,{ scope: "email" });
        // Ext.Viewport.animateActiveItem(this.referralpanel, this.slideLeftTransition);

    },
    //called when the Application is launched, remove if not needed
    launch : function(app) {

    document.addEventListener("deviceready", function(){
                     if ((typeof cordova == 'undefined') && (typeof Cordova == 'undefined')) alert('Cordova variable does not exist. Check that you have included cordova.js correctly');
            if (typeof CDV == 'undefined') alert('CDV variable does not exist. Check that you have included cdv-plugin-fb-connect.js correctly');
            if (typeof FB == 'undefined') alert('FB variable does not exist. Check that you have included the Facebook JS SDK file.');
            
    FB.init({ appId: "148296698666971", nativeInterface: CDV.FB, useCachedDialogs: false });
    }, false);
    },
    init : function() {
    
      
   //  this.onFacebookInit();
   
    },
    onFacebookInit : function() {

        var me = this;
        FB.init({ appId: "ap148296698666971pid", nativeInterface: CDV.FB, useCachedDialogs: false });
      

        FB.Event.subscribe('auth.logout', Ext.bind(me.onLogout, me));

        FB.getLoginStatus(function(response) {
           
            clearTimeout(me.fbLoginTimeout);

            me.hasCheckedStatus = true;
            Ext.Viewport.setMasked(false);

            Ext.get('splashLoader').destroy();
            Ext.get('rwf-body').addCls('greyBg');

            if (response.status == 'connected') {
                me.onLogin(response.authResponse.userID, response.authResponse.accessToken);
            } else {          
                me.login();
            }
        });

        me.fbLoginTimeout = setTimeout(function() {

            Ext.Viewport.setMasked(false);

            Ext.create('Ext.MessageBox', {
                title : 'Facebook Error',
                message : ['Facebook Authentication is not responding. ', 'Please check your Facebook app is correctly configured, ', 'then check the network log for calls to Facebook for more information.', 'Restart the app to try again.'].join('')
            }).show();

        }, 10000);
    },
    login : function() {
        Ext.Viewport.setMasked(false);
        var splash = Ext.getCmp('login');
        if (!splash) {
            //    Ext.Viewport.add({ xclass: 'JWF.view.Login', id: 'login' });
        }
        Ext.getCmp('login').showLoginText();
    },
    onLogin : function(userID, accessToken) {
  
  
        var me = this, errTitle;

        FB.api('/me', function(response) {

            if (response.error) {
                FB.logout();

                errTitle = "Facebook " + response.error.type + " error";
                Ext.Msg.alert(errTitle, response.error.message, function() {
                    me.login();
                });
            } else {

        //     console.log(userID+ "  ###  " +accessToken);
        
         if (!me.dashboard) {
         me.dashboard = Ext.create('bShift.view.Dashboard', {
         id: 'dashboard'
         });
         }
                Ext.Viewport.setActiveItem(me.dashboard);
                var referralsStore = Ext.getStore('Referrals');
                Ext.getStore('Referrals').getProxy().setExtraParam('user_id',userID);    
                Ext.getStore('Referrals').getProxy().setExtraParam('access_token',accessToken);

                
                referralsStore.load({
                  callback: function(records, operation, success) {
                        console.log(records);
                        }, scope: this} );    
          //      Ext.getStore('Runs').load();
            }
        });
    },
    logout : function() {
        Ext.Viewport.setMasked({
            xtype : 'loadmask',
            message : 'Logging out...'
        });
        FB.logout();
    },

    onLogout : function() {

        if (!this.hasCheckedStatus)
            return;

        this.login();

        Ext.Viewport.setMasked(false);
        Ext.Viewport.setActiveItem(Ext.getCmp('login'));
        Ext.getStore('Runs').removeAll();

        this.logoutCmp.destroy();
    },


    onUserTap : function(cmp) {

        if (!this.logoutCmp) {
            this.logoutCmp = Ext.create('Ext.Panel', {
                width : 120,
                top : 0,
                left : 0,
                padding : 5,
                modal : true,
                hideOnMaskTap : true,
                items : [{
                    xtype : 'button',
                    id : 'logoutButton',
                    text : 'Logout',
                    ui : 'decline'
                }]
            });
        }

        this.logoutCmp.showBy(cmp);
    }
}); 