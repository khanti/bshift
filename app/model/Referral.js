Ext.define('bShift.model.Referral', {
    extend: 'Ext.data.Model',
    
    config: {
            idProperty:'referralid',
        fields: [
            {name: 'referralid', type: 'int'},
            {name: 'userid', type: 'int'},
            {name: 'organizationid', type: 'int'},
            {name: 'teamid', type: 'int'},
            {name: 'referringdate', type: 'date'},
            {name: 'firstname', type: 'string'},
            {name: 'lastname', type: 'string'},
            {name: 'phonenumber', type: 'string'},
            {name: 'email', type: 'string'},
            {name: 'address1', type: 'string'},
            {name: 'address2', type: 'string'},
            {name: 'city', type: 'string'},
            {name: 'state', type: 'string'},
            {name: 'zipcode', type: 'string'},
            {name: 'photo64', type: 'string'},
            {name: 'description', type: 'string'},
            {name: 'latitude', type: 'float'},
            {name: 'longitude', type: 'float'},
            {name: 'HVAC', type: 'bool'},
            {name: 'Windows', type: 'bool'},
            {name: 'HEA', type: 'bool'},
            {name: 'Solar', type: 'bool'},
            {name: 'Weather', type: 'bool'},
            {name: 'Other', type: 'bool'}
            
            
            
            
            
            
        ]  ,
        validations:[
            { type:'presence', 'field':'firstname',message:'First Name Missing'},
            { type:'presence', 'field':'lastname',message:'Last Name Missing'},
            { type:'presence', 'field':'phonenumber',message:'Phone Number Missing'}
        ]
     
    }
});