var app = require('express').createServer(),

        express = require('express'),
        graph     = require('fbgraph'),
        referrals = require('./routes/referrals')     ;
   
referrals.setGraph(graph);   
        
var conf = {
    client_id:      '148296698666971'
  , client_secret:  'db8a809b253d90070d8e7e79fbf27d4c'
  , scope:          'email, user_about_me, user_birthday, user_location, publish_stream'
  , redirect_uri:   'http://localhost:3000/auth/facebook'
};


app.configure(function () {
    
 app.use(function(req, res, next) {
    var oneof = false;
    if(req.headers.origin) {
        res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
        oneof = true;
    }
    if(req.headers['access-control-request-method']) {
        res.setHeader('Access-Control-Allow-Methods', req.headers['access-control-request-method']);
        oneof = true;
    }
    if(req.headers['access-control-request-headers']) {
        res.setHeader('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
        oneof = true;
    }
    if(oneof) {
        res.setHeader('Access-Control-Max-Age', 60 * 60 * 24 * 365);
    }

    // intercept OPTIONS method
    if (oneof && req.method == 'OPTIONS') {
        res.send(200);
    }
    else {
        next();
    }
    });

    app.use(express.logger('dev'));     /* 'default', 'short', 'tiny', 'dev' */
    app.use(express.bodyParser());
      app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

var tester=0;

 
app.get('/', function(req, res) {
 
   res.send("TESTER:" + tester++);
   tester++;
});

app.get('/referrals', referrals.findAll);
app.get('/referrals/:id', referrals.findById);
app.post('/referrals', referrals.addReferral);

 
app.listen(process.env.VCAP_APP_PORT || 3000);
